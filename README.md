# SACCR Implementation

A simple web interface for the [open source R library](https://cran.r-project.org/web/packages/SACCR/index.html) around SA Counterparty Credit Risk under Basel III, the source of which is available [here](https://github.com/sa-ccr/SACCR). Kudos to the original author. 

The app is built using Shiny and needs a few dependencies:

```
install.packages("jsonlite")
install.packages("saccr")
install.packages("shiny")
```

Run it with

```
runApp("saccr")
```

